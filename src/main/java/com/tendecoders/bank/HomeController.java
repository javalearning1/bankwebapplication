package com.tendecoders.bank;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class HomeController {
	int previousTransaction;
	@Autowired
	private BankAccount bankAccount;
	
	@GetMapping("/")
	public String index() {
		return "index";
	}
	
	@PostMapping("/login")
	public String login(String userName,String password) {
		System.out.println(userName + password);
		   try {
	            bankAccount.validate(userName, password);
	            //bankAccount.menuOptions();
	           // MyAccount myac = new MyAccount();
	            //myac.accountM();
	        }catch (Exception e){
	            System.out.println(e.getMessage());
	            return "loginerror";
	        }
		   return "loginsuccess";
	}
	
	@GetMapping("/balance")
	public String balance(Model model) {		 
		 model.addAttribute("balanceAmt", bankAccount.balance());
		return "balance";
	}
	
	@GetMapping("/menu")
	public String menu(Model model) {
		return "menu";
	}
	
	@GetMapping("/deposit")
	public String deposit(Model model) {
		    bankAccount.deposit(500);
		    model.addAttribute("balanceAmt", bankAccount.balance());
		return "balance";
	}
	
	@GetMapping("/withdrawal")
	public String withdrawal(Model model) {
		 bankAccount.withdrawal(500);
		    model.addAttribute("balanceAmt", bankAccount.balance());
		return "balance";
	}
	
		
	@GetMapping("/transaction")
	public String transactionHistory(Model model) {
		bankAccount.transactionHistory();
	    model.addAttribute("balanceAmt", bankAccount.balance());
	return "balance";
		
	}
	
	@GetMapping("/exit")
	public String exit() {	

	return "index";
	}
	
	
	
	

}
