package com.tendecoders.bank;

public interface BankAccount {
	
	    void validate(String userName, String password) throws Exception;
	    void deposit(int amount);
	    void withdrawal(int amount);
	    void transactionHistory();
	    void menuOptions();	
	    Integer balance();


}
