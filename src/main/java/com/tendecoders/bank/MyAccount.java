package com.tendecoders.bank;

import java.util.Scanner;

public class MyAccount {
    char option=0;

    enum myAccount{
        FIXED_DEPOSIT, RECURRING_DEPOSIT, OTHER_DEPOSIT, POCKETS_WALLET, SAVINGS_ACCOUNT, TO_EXIT;
    }
    void accountM(){
        System.out.println("To see MyAccount Deposit choose one of the option below");
        System.out.println("1.FIXED_DEPOSIT");
        System.out.println("2.RECURRING_DEPOSIT");
        System.out.println("3.OTHER_DEPOSIT");
        System.out.println("4.POCKETS_WALLET");
        System.out.println("5.SAVINGS_ACCOUNT");
        System.out.println("6.To_EXIT");
        System.out.println("----------------------------------");
        Scanner scanner = new Scanner(System.in);
        option = scanner.next().charAt(0);

        switch(option) {
            case '1':
                System.out.println("Fixed Deposit amount can be seen here");
                break;
            case '2':
                System.out.println("Recurring Deposit amount can be seen here");
                break;
            case '3':
                System.out.println("Other Deposit amount can be seen here");
                break;
            case '4':
                System.out.println("Amount of pocket wallet");
                break;
            case '5':
                System.out.println("Savings account value can be seen here ");
                break;
            case '6':
                System.out.println("Thank you for choosing our bank");
                break;
            default:
                System.out.println("choose correct option");
        }

    }

}

