package com.tendecoders.bank;


import java.util.Scanner;

import org.springframework.stereotype.Service;

@Service
public class BankAccountImpl extends UserValidation {

    int balance=1000;
    int previousTransaction;
    private int accountNumber;

   public void deposit(int amount){
        if (amount != 0) {
            balance = balance + amount;
            previousTransaction = amount;
        }
    }
    public void withdrawal(int amount2){
        if (amount2 != 0) {
            balance = balance - amount2;
            previousTransaction = -amount2;
        }
    }
   public void transactionHistory(){
        if (previousTransaction > 0) {
            System.out.println("Deposited amount is " + previousTransaction);
        } else if(previousTransaction < 0) {
            System.out.println("Withdrawal is "+Math.abs(previousTransaction));
        } else {
            System.out.println("No transaction");
        }
    }
    public void menuOptions(){

        int option = '0';

        Scanner scanner = new Scanner(System.in);
        System.out.println("Welcome "+getUserName());
        System.out.println("1.View Balance");
        System.out.println("2.Check Deposit");
        System.out.println("3.Withdrawal");
        System.out.println("4.See transaction History");
        System.out.println("5.Exit");
        do{
            System.out.println("Please choose one of the option");
            System.out.println("---------------------------------");
            System.out.println("\n");
            option = scanner.next().charAt(0);
            System.out.println("----------------------------------");
            switch(option){
                case '1':
                    System.out.println("Balance = "+balance);
                    break;
                case '2':
                    System.out.println("Enter an amount to deposit");
                    int amount = scanner.nextInt();
                    deposit(amount);
                    System.out.println("/n");
                    break;
                case '3':
                    System.out.println("Withdrawal");
                    int amount2 = scanner.nextInt();
                    withdrawal(amount2);
                    System.out.println("/n");
                    break;
                case '4':
                    System.out.println("Check transaction");
                    transactionHistory();
                    break;
                case '5':
                    System.out.println("****************");
                    break;
                default:
                    System.out.println("Please enter a valid number");

            }

        } while (option!= '5');
        System.out.println("Thank you");
        System.out.println("************************");
    }
	@Override
	public Integer balance() {
		return balance;
		
	}

}
